package pl.codementors.interfaces;

/**
 * Interface for Herbivores
 */

public interface Herbivore {

    void eatHerbs();
}

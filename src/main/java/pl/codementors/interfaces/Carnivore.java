package pl.codementors.interfaces;

/**
 * Interface for Carnivores
 */

public interface Carnivore {

    void eatMeat();
}

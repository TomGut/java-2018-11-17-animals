package pl.codementors;

import pl.codementors.Models.Mammal;
import pl.codementors.interfaces.Carnivore;

public class Woolf extends Mammal implements Carnivore {

    public void howl(){
        System.out.println("HOWL");
    }

    public void eatMeat() {
        System.out.println("EAT MEAT");
    }

    @Override
    public void eat(){
        System.out.println("WOOLF EATS");
    }

}

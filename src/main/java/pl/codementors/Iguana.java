package pl.codementors;

import pl.codementors.Models.Lizard;
import pl.codementors.interfaces.Herbivore;

public class Iguana extends Lizard implements Herbivore {

    public void hiss(){
        System.out.println("HISS");
    }

    public void eatHerbs() {
        System.out.println("EAT HERBS");
    }

    @Override
    public void eat(){
        System.out.println("IGUANA EATS");
    }
}

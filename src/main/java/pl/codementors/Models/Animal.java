package pl.codementors.Models;

import java.io.Serializable;

/**
 * Abstract class for all animals
 */

public abstract class Animal implements Serializable {// for binary files i/o

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
}

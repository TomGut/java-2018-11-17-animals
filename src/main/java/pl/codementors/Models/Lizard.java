package pl.codementors.Models;

public abstract class Lizard extends Animal {

    private String shuckColor;

    public String getShuckColor() {
        return shuckColor;
    }

    public void setShuckColor(String shuckColor) {
        this.shuckColor = shuckColor;
    }
}

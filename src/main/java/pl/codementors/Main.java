package pl.codementors;

import pl.codementors.Models.Animal;
import pl.codementors.Models.Bird;
import pl.codementors.Models.Lizard;
import pl.codementors.Models.Mammal;
import pl.codementors.interfaces.Carnivore;
import pl.codementors.interfaces.Herbivore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * App use Inheritance and Conversion concepts to print out/save objects from array to console and txt files on HDD
 * @author Tomek Gutowski
 *
 */
public class Main {
    //empty array for methods readAnimals and readAnimalsBinary
    private static Object[] animals;

    public static void main (String[] args){
        System.out.println("HELLO IN THE ZOO");
        System.out.println();

        //static filepath for 3 types of txt files to keep track of changes more easily than in 1 file
        String path = "animals.txt";//input file
        String pathNewFile = "animalsNewFile.txt";//output file
        String pathBinary = "animalsBinary.txt";//binary operations

        //Reference variable for readAnimals method returned array
        Object[] animalTable = readAnimals(path);

        //save animals from animalTable to file on HDD
        saveAnimals(pathNewFile, animalTable);
        //print animals out from animalTable to console
        printAnimals(animalTable);
        //feed all animals
        feedAnimals(animalTable);
        //feed carnivore
        feedCarnivore(animalTable);
        //feed herbivore
        feedHerbivore(animalTable);
        //all animals which are Woolf class do howl
        animalHowl(animalTable);
        //all animals which are Iguana class do hiss
        animalHiss(animalTable);
        //all animals which are Parrot class do tweet
        animalTweet(animalTable);
        //save animals to binary file on HDD
        saveAnimalsBinary(pathBinary, animalTable);
        //load animals from binary file
        Object[] animalsFromBinary = readAnimalsBinary(pathBinary);
        //write down all animals from binary to console
        printAnimals(animalsFromBinary);
    }

    /**
     * Reads animals from binary file
     * @param path hardcoded filepath
     * @return array of animals
     */
    private static Object[] readAnimalsBinary(String path) {
        File file = new File(path);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            for(int i=0; i<animals.length; i++){
                if(animals[i] != null){
                    animals[i] = ois.readObject();
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
            return new Object[0];
        }
        return animals;
    }

    /**
     * Saving animals array to binary
     * @param path use hardcoded file path
     * @param array use array of animals
     */
    private static void saveAnimalsBinary(String path, Object[] array) {
        File file = new File(path);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            for(int i=0; i<array.length; i++){
                oos.writeObject(array[i]);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     *  All animals that are Parrot class do tweet
     *  @param array use array of animals
     */
    private static void animalTweet(Object[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                if (array[i] instanceof Parrot) {
                    ((Parrot) array[i]).tweet();
                }
            }
        }
    }

    /**
     *  All animals that are Iguana class do hiss
     *  @param array use array of animals
     */
    private static void animalHiss(Object[] array){
        for(int i=0; i<array.length; i++){
            if(array[i] != null){
                if(array[i] instanceof Iguana){
                    ((Iguana) array[i]).hiss();
                }
            }
        }
    }

    /**
     *  All animals that are Woolf class do howl
     *  @param array use array of animals
     */
    private static void animalHowl(Object[] array){
        for(int i=0; i<array.length; i++){
            if(array[i] != null){
                if(array[i] instanceof Woolf){
                    ((Woolf) array[i]).howl();
                }
            }
        }
    }

    /**
     *  Feed all herbivores
     *  @param array use array of animals
     */
    private static void feedHerbivore(Object[] array){
        for(int i=0; i<array.length; i++){
            if(array[i] != null){
                if(array[i] instanceof Herbivore){
                    ((Herbivore) array[i]).eatHerbs();
                }
            }
        }
    }

    /**
     *  Feed all carnivores
     *  @param array use animals array
     */
    private static void feedCarnivore(Object[] array){
        for(int i=0; i<array.length; i++){
            if(array[i] != null){
                if(array[i] instanceof Carnivore){
                    ((Carnivore) array[i]).eatMeat();
                }
            }
        }
    }

    /**
     *  Feed all animals
     *  @param array provide animal for type check up and type specific method eat() call
     */
    private static void feedAnimals(Object[] array){
        for(int i=0; i<array.length; i++){
            if(array[i] != null){
                ((Animal) array[i]).eat();
            }
        }

        /*
        //usage of method specified in Woolf,Iguana or Parrot class

        for(int i=0; i<array.length; i++){
            if (array[i] instanceof Mammal) {
                ((Woolf) array[i]).eat();
            }else if (array[i] instanceof Lizard) {
                ((Iguana) array[i]).eat();
            }else if (array[i] instanceof Bird) {
                ((Parrot) array[i]).eat();
            }
        }
        */
    }

    /**
     *  Prints out all animals with additional info to console
     *  @param array use array of animals
     */
    private static void printAnimals(Object[] array) {
        //starting values for loop
        String type = "";
        String name = "";
        int age = 0;
        String color = "";

        for (int i = 0; i < array.length; i++) {
            if(array[i] != null){
                //instance checkout and conversion
                if (array[i] instanceof Mammal) {
                    type = ((Woolf) array[i]).getClass().getSimpleName();
                    name = ((Woolf) array[i]).getName();
                    age = ((Woolf) array[i]).getAge();
                    color = ((Woolf) array[i]).getFurColor();
                }else if (array[i] instanceof Lizard) {
                    type = ((Iguana) array[i]).getClass().getSimpleName();
                    name = ((Iguana) array[i]).getName();
                    age = ((Iguana) array[i]).getAge();
                    color = ((Iguana) array[i]).getShuckColor();
                }else if (array[i] instanceof Bird) {
                    type = ((Parrot) array[i]).getClass().getSimpleName();
                    name = ((Parrot) array[i]).getName();
                    age = ((Parrot) array[i]).getAge();
                    color = ((Parrot) array[i]).getFeatherColor();
                }
                System.out.println("Typ zwierzęcia: " + type + "\n" + "Imię: " + name + "\n" + "Wiek: "
                        + age + "\n" + "Kolor: " + color + "\n");
            }
        }
    }

    /**
     *  Save animals to file on HDD
     *  @param path provide hardcoded path to input file
     *  @param array use array of animals
     */
    private static void saveAnimals(String path, Object[] array) {
        File newFile = new File(path);
        try (FileWriter fw = new FileWriter(newFile)) {
            //starting values for loop
            String type = "";
            String name = "";
            int age = 0;
            String color = "";

            for (int i = 0; i < array.length; i++) {
                if(array[i] != null){
                    //instance checkout and conversion
                    if (array[i] instanceof Mammal) {
                        type = ((Woolf) array[i]).getClass().getSimpleName();
                        name = ((Woolf) array[i]).getName();
                        age = ((Woolf) array[i]).getAge();
                        color = ((Woolf) array[i]).getFurColor();
                    }else if (array[i] instanceof Lizard) {
                        type = ((Iguana) array[i]).getClass().getSimpleName();
                        name = ((Iguana) array[i]).getName();
                        age = ((Iguana) array[i]).getAge();
                        color = ((Iguana) array[i]).getShuckColor();
                    }else if (array[i] instanceof Bird) {
                        type = ((Parrot) array[i]).getClass().getSimpleName();
                        name = ((Parrot) array[i]).getName();
                        age = ((Parrot) array[i]).getAge();
                        color = ((Parrot) array[i]).getFeatherColor();
                    }
                    //writing values down for txt file
                    fw.write(type + "\n" + name + "\n" + age + "\n" + color + "\n");
                }
            }
        }catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     *  Read animals for file on HDD
     *  @param path provide hardcoded path to file
     *  @return Object array of animals signed to animalTable in main method
     */
     private static Object[] readAnimals(String path) {
         File file = new File(path);
         try (BufferedReader br = new BufferedReader(new FileReader(file))) {
             //getting index for array
             String animalsNo = br.readLine();
             int index = Integer.parseInt(animalsNo);
             //setting up array size with index param
             animals = new Object[index];

             for (int i = 0; i < animals.length; i++) {
                 String type = br.readLine();
                 String name = br.readLine();
                 String years = br.readLine();
                 String color = br.readLine();
                 int yearsInt = Integer.parseInt(years);

                     switch (type) {
                         //if specific animal matched - new object created and put in into array that will be returned
                         case ("Woolf"): {
                             Woolf woolf = new Woolf();
                             woolf.setName(name);
                             woolf.setAge(yearsInt);
                             woolf.setFurColor(color);

                             animals[i] = woolf;
                             break;
                         }
                         case ("Lizard"): {
                             Iguana iguana = new Iguana();
                             iguana.setName(name);
                             iguana.setAge(yearsInt);
                             iguana.setShuckColor(color);

                             animals[i] = iguana;
                             break;
                         }
                         case ("Parrot"): {
                             Parrot parrot = new Parrot();
                             parrot.setName(name);
                             parrot.setAge(yearsInt);
                             parrot.setFeatherColor(color);

                             animals[i] = parrot;
                             break;
                         }
                     }
             }
         } catch (IOException ex) {
             System.err.println(ex);
             return new Object[0];
         }
         return animals;
     }
}
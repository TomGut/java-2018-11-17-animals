package pl.codementors;

import pl.codementors.Models.Bird;
import pl.codementors.interfaces.Herbivore;

public class Parrot extends Bird implements Herbivore {

    public void tweet(){
        System.out.println("TWEET");
    }

    public void eatHerbs() {
        System.out.println("EAT HERBS");
    }

    @Override
    public void eat(){
        System.out.println("PARROT EATS");
    }
}
